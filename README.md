# mankey

A Rust implementation of the [Monkey Interpreter](https://monkeylang.org/). Will implement all features and tests in the book, _Writing an Interpreter in Go_ by Thorsten Ball. It is very much in progress and is not very usuable right now.

### Progress
Note: Based on the major sections in the book and their subsections.
- [X] Lexing (5/5)
- [] Parsing (7/9)
- [] Evaluating (0/11)
- [] Extending (0/6)
- [] Macros

### Tests

Perform tests using cargo:

```
cargo test
```

### Extensions / Changes

Features added/changed that were not in the book. Currently, none of these are done but I plan to attempt to add:

- UTF8 support
- Modulus division
- Floats
- Else-if operator
- Match statements
- Type/Impl statements

