use std::fmt;

use crate::tokens::Token;

#[derive(Debug, Clone, PartialEq)]
pub struct Program {
    pub statements: Vec<Statement>,
}

impl fmt::Display for Program {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), ::std::fmt::Error> {
        let mut output = String::new();

        for (line, stmt) in self.statements.iter().enumerate() {
            output += &stmt.to_string();
            if self.statements.len() - line != 1 {
                output += &"\n".to_string();
            }
        }

        write!(f, "{}", output)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Statement {
    Let(Identifier, Expression),
    Return(Expression),
    Expression(Expression),
    None,
}

impl fmt::Display for Statement {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), ::std::fmt::Error> {
        match self {
            Statement::Let(id, expr) => write!(f, "let {} = {:?}", id, expr.to_string()),
            Statement::Return(expr) => write!(f, "return {}", expr.to_string()),
            Statement::Expression(expr) => write!(f, "{}", expr.to_string()),
            _ => write!(f, "None"),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Expression {
    Bool(bool),
    Identifier(Identifier),
    Integer(i32),
    String(String),
    Prefix(String, Box<Expression>), // operator, right side
    Infix(Token, Box<Expression>, Box<Expression>), // operator, left side, right side
    None,
}

impl fmt::Display for Expression {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), ::std::fmt::Error> {
        match self {
            Expression::Bool(val) => write!(f, "{}", val),
            Expression::String(val) => write!(f, "{}", val.clone()),
            Expression::Identifier(val) => write!(f, "{}", val),
            Expression::Integer(int) => write!(f, "{}", int),
            Expression::Prefix(op, expression) => write!(f, "({}{})", op, expression.to_string()),
            Expression::Infix(operator, left, right) => write!(
                f,
                "({} {} {})",
                left.to_string(),
                operator.to_string(),
                right.to_string()
            ),
            _ => write!(f, "None"),
        }
    }
}

pub type Identifier = String;
