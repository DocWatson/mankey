use crate::tokens::{lookup_identifier, Token, TokenType};

pub struct Lexer {
    input: String,
    /// the input as a vec of characters, for accessing the chars by position
    input_chars: Vec<char>,
    /// current position in input
    position: usize,
    /// current reading position in input
    read_position: usize,
    /// current character under examination
    chr: char,
}

impl Lexer {
    pub fn new(input: String) -> Lexer {
        let mut lexer = Lexer {
            input: input.clone(),                 // clone here to avoid borrow conflicts
            input_chars: input.chars().collect(), // collect the chars into a vec for easy reading/peeking
            position: 0,
            read_position: 0,
            chr: '\0',
        };

        lexer.read_char();
        lexer
    }

    ///check the next character in the input without updating the lexer state
    pub fn peek_char(&mut self) -> char {
        if self.read_position < self.input.len() {
            return self.input_chars[self.read_position];
        }

        '\0'
    }

    /// get the next character and advance the position in the input string
    pub fn read_char(&mut self) {
        if self.read_position >= self.input.len() {
            // nothing read yet or at end of file
            self.chr = '\0';
        } else {
            // set the character to the current read position
            self.chr = self.input_chars[self.read_position];
        }

        // update the read positions
        self.position = self.read_position;
        self.read_position += 1;
    }

    /// Skips whitespace. spaces are only relevant for separating identifiers and tokens. Mankey is whitespace agnostic
    pub fn skip_whitespace(&mut self) {
        while self.chr.is_ascii_whitespace() {
            self.read_char()
        }
    }

    pub fn next_token(&mut self) -> Token {
        self.skip_whitespace();
        let token = match self.chr {
            '=' => {
                if self.peek_char() == '=' {
                    self.read_char();
                    self.chr = self.peek_char();
                    return Token {
                        token_type: TokenType::EQ,
                        literal: "==".to_string(),
                    };
                }

                Token {
                    token_type: TokenType::ASSIGN,
                    literal: self.chr.to_string(),
                }
            }
            '+' => Token {
                token_type: TokenType::PLUS,
                literal: self.chr.to_string(),
            },
            '(' => Token {
                token_type: TokenType::LPAREN,
                literal: self.chr.to_string(),
            },
            ')' => Token {
                token_type: TokenType::RPAREN,
                literal: self.chr.to_string(),
            },
            '{' => Token {
                token_type: TokenType::LBRACE,
                literal: self.chr.to_string(),
            },
            '}' => Token {
                token_type: TokenType::RBRACE,
                literal: self.chr.to_string(),
            },
            ',' => Token {
                token_type: TokenType::COMMA,
                literal: self.chr.to_string(),
            },
            ';' => Token {
                token_type: TokenType::SEMICOLON,
                literal: self.chr.to_string(),
            },
            '\0' => Token {
                token_type: TokenType::EOF,
                literal: "".to_string(),
            },
            '!' => {
                if self.peek_char() == '=' {
                    self.read_char();
                    self.chr = self.peek_char();
                    return Token {
                        token_type: TokenType::NOTEQ,
                        literal: "!=".to_string(),
                    };
                }
                Token {
                    token_type: TokenType::BANG,
                    literal: self.chr.to_string(),
                }
            }
            '-' => Token {
                token_type: TokenType::MINUS,
                literal: self.chr.to_string(),
            },
            '/' => Token {
                token_type: TokenType::SLASH,
                literal: self.chr.to_string(),
            },
            '*' => Token {
                token_type: TokenType::ASTERISK,
                literal: self.chr.to_string(),
            },
            '%' => Token {
                token_type: TokenType::MODULO,
                literal: self.chr.to_string(),
            },
            '>' => Token {
                token_type: TokenType::GT,
                literal: self.chr.to_string(),
            },
            '<' => Token {
                token_type: TokenType::LT,
                literal: self.chr.to_string(),
            },
            _ => {
                if is_letter(self.chr) {
                    let literal = self.read_identifier();
                    let token_type = lookup_identifier(literal);
                    return Token {
                        token_type: token_type,
                        literal: literal.to_string(),
                    };
                } else if is_digit(self.chr) {
                    return Token {
                        token_type: TokenType::INT,
                        literal: self.read_number().to_string(),
                    };
                }
                Token {
                    token_type: TokenType::ILLEGAL,
                    literal: self.chr.to_string(),
                }
            }
        };

        // read the next char and return the token
        self.read_char();
        token
    }

    pub fn read_number(&mut self) -> &str {
        // get the start position
        let position = self.position;

        // loop as long as the input is a valid letter
        while is_digit(self.chr) {
            self.read_char();
        }

        // the position in `self` is the last step of the loop
        // effectively gets an entire word
        &self.input[position..self.position]
    }

    /// gets an identifier (i.e. a variable name)
    pub fn read_identifier(&mut self) -> &str {
        // get the start position
        let position = self.position;

        // loop as long as the input is a valid letter
        while is_letter(self.chr) {
            self.read_char();
        }

        // the position in `self` is the last step of the loop
        // effectively gets an entire word
        &self.input[position..self.position]
    }
}

// helper function that takes a character and determines if it is alphabetic
// we do this so that we can parse identifiers more easily
// note: _ is considered a legal letter in identifiers
fn is_letter(input: char) -> bool {
    input.is_alphabetic() || input == '_'
}

// helper function that takes a character and determines if it is numeric
fn is_digit(input: char) -> bool {
    input.is_digit(10)
}
