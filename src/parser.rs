use crate::ast::{Expression, Program, Statement};
use crate::lexer::Lexer;
use crate::tokens::{Precedence, Token, TokenType};

use std::mem;

pub struct Parser {
    lexer: Lexer,
    current_token: Token,
    peek_token: Token,
    errors: Vec<String>,
}

impl Parser {
    pub fn new(lexer: Lexer) -> Parser {
        let default_token = Token {
            token_type: TokenType::ILLEGAL,
            literal: "".to_string(),
        };

        let mut parser = Parser {
            lexer: lexer,
            current_token: default_token.clone(),
            peek_token: default_token.clone(),
            errors: vec![],
        };

        // read two tokens to initialize current_token, then peek_token
        parser.next_token();
        parser.next_token();

        parser
    }

    /// Returns all the parser errors encountered
    pub fn errors(&self) -> Vec<String> {
        self.errors.clone()
    }

    // Advances the token reading by setting the current token to the peek token and having the lexer advance its tokens
    fn next_token(&mut self) {
        self.current_token = mem::replace(&mut self.peek_token, self.lexer.next_token())
    }

    /// Checks the next token for a given type
    /// On successful match, advance the tokens before returning true
    fn expect_peek(&mut self, token: TokenType) -> bool {
        if self.peek_token_is(token) {
            self.next_token();
            return true;
        }

        let error = format!(
            "Mankey expected token {:?}, but encountered {:?}. Mankey charges at you!",
            token, self.peek_token.token_type
        );
        self.errors.push(error);

        return false;
    }

    /// Helper method to check the current token's type
    fn current_token_is(&mut self, token: TokenType) -> bool {
        self.current_token.token_type == token
    }

    /// Helper Method to check the next token's type
    fn peek_token_is(&mut self, token: TokenType) -> bool {
        self.peek_token.token_type == token
    }
    /// Matches on the current token type and calls the appropriate parser method
    fn parse_statement(&mut self) -> Statement {
        match self.current_token.token_type {
            TokenType::LET => self.parse_let_statement(),
            TokenType::RETURN => self.parse_return_statement(),
            _ => self.parse_expression_statement(),
        }
    }

    /// Parses `let` statements
    /// Checks for an identifier, an addignment operator, an expression and a semicolon
    fn parse_let_statement(&mut self) -> Statement {
        let mut stmt = Statement::None;
        if !self.expect_peek(TokenType::IDENT) {
            return stmt;
        }

        let ident = self.current_token.literal.clone();

        if !self.expect_peek(TokenType::ASSIGN) {
            return stmt;
        }

        self.parse_to_semicolon();

        // todo: get actual value here
        stmt = Statement::Let(ident, Expression::Integer(2));
        stmt
    }

    fn parse_return_statement(&mut self) -> Statement {
        self.next_token();
        // TODO: make sure this works...
        self.parse_to_semicolon();

        let return_value = self.current_token.literal.clone();

        // TODO: get actual value here
        Statement::Return(Expression::String(return_value))
    }

    /// Parses all other statements besides `let`s and `return`s
    fn parse_expression_statement(&mut self) -> Statement {
        let expr = self.parse_expression(Precedence::LOWEST);
        // check for optional semicolon
        if self.peek_token_is(TokenType::SEMICOLON) {
            self.next_token();
        }

        match expr {
            Expression::None => Statement::None,
            _ => Statement::Expression(expr),
        }
    }

    fn parse_expression(&mut self, priority: Precedence) -> Expression {
        // check for a prefix
        let prefix_expression = self.parse_prefix(self.current_token.token_type);

        let mut left_expression = match prefix_expression {
            Expression::None => {
                self.errors.push(format!(
                    "Mankey did not find a PREFIX for {:?}. ",
                    self.current_token.token_type
                ));
                return Expression::None;
            }
            _ => prefix_expression,
        };

        // loop through this as long as:
        // - the given priority is less than the next token's precedence
        // - the current token is not the lowest precedence
        // - the next token is not a semicolon
        //
        // this is is an elegant way to capture infix expressions by leaning on
        // token precedences
        while !self.peek_token_is(TokenType::SEMICOLON)
            && priority < self.peek_token.get_precendence()
            && self.peek_token.get_precendence() != Precedence::LOWEST
        {
            // advance the token and parse it as an infix
            self.next_token();
            left_expression = self.parse_infix(left_expression);

            println!("{:?}", left_expression);
        }

        left_expression
    }

    fn parse_prefix(&mut self, token_type: TokenType) -> Expression {
        match token_type {
            TokenType::IDENT => self.parse_identifier(),
            TokenType::INT => self.parse_integer_literal(),
            TokenType::BANG | TokenType::MINUS => self.parse_prefix_expression(),
            _ => Expression::None,
        }
    }

    fn parse_infix(&mut self, left: Expression) -> Expression {
        // TODO: add paren and bracket types and methods
        match self.current_token.token_type {
            _ => self.parse_infix_expression(left),
        }
    }

    /// parse_prefix_expression parses expressions that are prefixed by the
    /// BANG (!) or MINUS (-) operaator
    fn parse_prefix_expression(&mut self) -> Expression {
        // the oprrator in question (either ! or -)
        // because self is a mutable reference, we must clone it to use it
        let operator = self.current_token.literal.clone();

        // because we know the current token is a prefix token (otherwise this function
        // would not have been called), we must advance to the next token to see on what
        // token the operator is acting
        self.next_token();

        let right = self.parse_expression(Precedence::PREFIX);
        if right == Expression::None {
            self.errors.push(format!(
                "Mankey found no expression for prefix operator {}",
                operator
            ));
        };

        Expression::Prefix(operator, Box::new(right))
    }

    /// parse_infix_expression parses expressions that use infix operators
    /// such as math symbols and comparisons
    fn parse_infix_expression(&mut self, left: Expression) -> Expression {
        let precedence = self.current_token.get_precendence();
        let operator = self.current_token.clone();

        self.next_token();

        let right = self.parse_expression(precedence);

        Expression::Infix(operator, Box::new(left), Box::new(right))
    }

    fn parse_identifier(&mut self) -> Expression {
        let illegal = Token {
            token_type: TokenType::ILLEGAL,
            literal: "".to_string(),
        };
        let cur_token = mem::replace(&mut self.current_token, illegal);

        Expression::Identifier(cur_token.literal.to_string())
    }

    fn parse_integer_literal(&mut self) -> Expression {
        let value = self.current_token.literal.parse();

        match value {
            Ok(expr) => Expression::Integer(expr),
            Err(e) => {
                self.errors.push(e.to_string());
                Expression::None
            }
        }
    }

    /// Helper method to continue collecting tokens until the parser encounters a semicolon
    fn parse_to_semicolon(&mut self) {
        while !self.current_token_is(TokenType::SEMICOLON) {
            if self.current_token_is(TokenType::EOF) {
                self.errors.push(
                    "Mankey encountered EOF while looking for a semicolon. MANKEY IS ENRAGED."
                        .to_string(),
                );
                break;
            }
            self.next_token();
        }
    }

    pub fn parse_program(&mut self) -> Program {
        let mut statements = vec![];

        loop {
            match self.current_token.token_type {
                TokenType::EOF => break,
                _ => {
                    let stmt = self.parse_statement();
                    match stmt {
                        Statement::None => (),
                        _ => {
                            statements.push(stmt);
                        }
                    }
                    self.next_token();
                }
            }
        }

        if self.current_token.token_type != TokenType::EOF {
            let stmt = self.parse_statement();
            if stmt != Statement::None {
                statements.push(stmt);
            }
            self.next_token();
        }

        Program {
            statements: statements,
        }
    }
}
