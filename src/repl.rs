use std::io::{self};

use crate::lexer::Lexer;
use crate::tokens::TokenType;

const PROMPT: &str = "~~> ";

pub fn start() {
    println!("MANKEY IS PLEASED TO SEE YOU");

    loop {
        // Print prompt and flush to write it to console
        print!("{}", PROMPT);
        io::Write::flush(&mut io::stdout()).expect("flush failed!");

        // Scan Input line
        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Err(error) => {
                println!("error: {}", error);
                return;
            }
            Ok(_) => (),
        }

        let mut lex = Lexer::new(input);
        loop {
            let token = lex.next_token();
            println!("{:?}", token);

            if token.token_type == TokenType::EOF {
                break;
            }
        }
    }
}
