use crate::lexer::Lexer;
use crate::tokens::{Token, TokenType};
#[test]
fn test_next_token() {
    let input = "=+(){},;";

    let expected = [
        Token::new(TokenType::ASSIGN, "=".to_string()),
        Token::new(TokenType::PLUS, "+".to_string()),
        Token::new(TokenType::LPAREN, "(".to_string()),
        Token::new(TokenType::RPAREN, ")".to_string()),
        Token::new(TokenType::LBRACE, "{".to_string()),
        Token::new(TokenType::RBRACE, "}".to_string()),
        Token::new(TokenType::COMMA, ",".to_string()),
        Token::new(TokenType::SEMICOLON, ";".to_string()),
    ];

    let mut lexer = Lexer::new(input.to_string());

    for token in expected.iter() {
        let lexed_token = lexer.next_token();
        assert_eq!(lexed_token, *token);
    }
}

#[test]
fn test_monkey_simple() {
    let input = "let five =5;
        let ten = 10;
        
        let add = fn(x, y) {
          x + y;
        };
        
        let result = add(five, ten);
        ";
    let expected = [
        Token::new(TokenType::LET, "let".to_string()),
        Token::new(TokenType::IDENT, "five".to_string()),
        Token::new(TokenType::ASSIGN, "=".to_string()),
        Token::new(TokenType::INT, "5".to_string()),
        Token::new(TokenType::SEMICOLON, ";".to_string()),
        Token::new(TokenType::LET, "let".to_string()),
        Token::new(TokenType::IDENT, "ten".to_string()),
        Token::new(TokenType::ASSIGN, "=".to_string()),
        Token::new(TokenType::INT, "10".to_string()),
        Token::new(TokenType::SEMICOLON, ";".to_string()),
        Token::new(TokenType::LET, "let".to_string()),
        Token::new(TokenType::IDENT, "add".to_string()),
        Token::new(TokenType::ASSIGN, "=".to_string()),
        Token::new(TokenType::FUNCTION, "fn".to_string()),
        Token::new(TokenType::LPAREN, "(".to_string()),
        Token::new(TokenType::IDENT, "x".to_string()),
        Token::new(TokenType::COMMA, ",".to_string()),
        Token::new(TokenType::IDENT, "y".to_string()),
        Token::new(TokenType::RPAREN, ")".to_string()),
        Token::new(TokenType::LBRACE, "{".to_string()),
        Token::new(TokenType::IDENT, "x".to_string()),
        Token::new(TokenType::PLUS, "+".to_string()),
        Token::new(TokenType::IDENT, "y".to_string()),
        Token::new(TokenType::SEMICOLON, ";".to_string()),
        Token::new(TokenType::RBRACE, "}".to_string()),
        Token::new(TokenType::SEMICOLON, ";".to_string()),
        Token::new(TokenType::LET, "let".to_string()),
        Token::new(TokenType::IDENT, "result".to_string()),
        Token::new(TokenType::ASSIGN, "=".to_string()),
        Token::new(TokenType::IDENT, "add".to_string()),
        Token::new(TokenType::LPAREN, "(".to_string()),
        Token::new(TokenType::IDENT, "five".to_string()),
        Token::new(TokenType::COMMA, ",".to_string()),
        Token::new(TokenType::IDENT, "ten".to_string()),
        Token::new(TokenType::RPAREN, ")".to_string()),
        Token::new(TokenType::SEMICOLON, ";".to_string()),
        Token::new(TokenType::EOF, "".to_string()),
    ];

    let mut lexer = Lexer::new(input.to_string());

    for token in expected.iter() {
        let lexed_token = lexer.next_token();
        assert_eq!(lexed_token, *token);
    }
}

#[test]
pub fn test_gibberish() {
    let input = "
        !-/*5;
        5 < 10 > 5;
        ";

    let expected = [
        Token::new(TokenType::BANG, "!".to_string()),
        Token::new(TokenType::MINUS, "-".to_string()),
        Token::new(TokenType::SLASH, "/".to_string()),
        Token::new(TokenType::ASTERISK, "*".to_string()),
        Token::new(TokenType::INT, "5".to_string()),
        Token::new(TokenType::SEMICOLON, ";".to_string()),
        Token::new(TokenType::INT, "5".to_string()),
        Token::new(TokenType::LT, "<".to_string()),
        Token::new(TokenType::INT, "10".to_string()),
        Token::new(TokenType::GT, ">".to_string()),
        Token::new(TokenType::INT, "5".to_string()),
        Token::new(TokenType::SEMICOLON, ";".to_string()),
        Token::new(TokenType::EOF, "".to_string()),
    ];

    let mut lexer = Lexer::new(input.to_string());

    for token in expected.iter() {
        let lexed_token = lexer.next_token();
        assert_eq!(lexed_token, *token);
    }
}

#[test]
pub fn test_if_else() {
    let input = "
        if (5 < 10) {
            return true;
        } else {
            return false;
        }
        ";

    let expected = [
        Token::new(TokenType::IF, "if".to_string()),
        Token::new(TokenType::LPAREN, "(".to_string()),
        Token::new(TokenType::INT, "5".to_string()),
        Token::new(TokenType::LT, "<".to_string()),
        Token::new(TokenType::INT, "10".to_string()),
        Token::new(TokenType::RPAREN, ")".to_string()),
        Token::new(TokenType::LBRACE, "{".to_string()),
        Token::new(TokenType::RETURN, "return".to_string()),
        Token::new(TokenType::TRUE, "true".to_string()),
        Token::new(TokenType::SEMICOLON, ";".to_string()),
        Token::new(TokenType::RBRACE, "}".to_string()),
        Token::new(TokenType::ELSE, "else".to_string()),
        Token::new(TokenType::LBRACE, "{".to_string()),
        Token::new(TokenType::RETURN, "return".to_string()),
        Token::new(TokenType::FALSE, "false".to_string()),
        Token::new(TokenType::SEMICOLON, ";".to_string()),
        Token::new(TokenType::RBRACE, "}".to_string()),
        Token::new(TokenType::EOF, "".to_string()),
    ];

    let mut lexer = Lexer::new(input.to_string());

    for token in expected.iter() {
        let lexed_token = lexer.next_token();
        assert_eq!(lexed_token, *token);
    }
}

#[test]
pub fn test_equal_comparisons() {
    let input = "
        10 == 10;
        10 != 9
        ";

    let expected = [
        Token::new(TokenType::INT, "10".to_string()),
        Token::new(TokenType::EQ, "==".to_string()),
        Token::new(TokenType::INT, "10".to_string()),
        Token::new(TokenType::SEMICOLON, ";".to_string()),
        Token::new(TokenType::INT, "10".to_string()),
        Token::new(TokenType::NOTEQ, "!=".to_string()),
        Token::new(TokenType::INT, "9".to_string()),
        Token::new(TokenType::EOF, "".to_string()),
    ];

    let mut lexer = Lexer::new(input.to_string());

    for token in expected.iter() {
        let lexed_token = lexer.next_token();
        assert_eq!(lexed_token, *token);
    }
}
