use crate::ast::{Expression, Statement};
use crate::lexer::Lexer;
use crate::parser::Parser;
use crate::tokens::{Token, TokenType};

//---------------------
// Let Statements
/// --------------------
#[test]
fn test_let_statement() {
    let input = "
let x = 5;
let y = 10;
let foobar = 838383;
";

    let lexer = Lexer::new(input.to_string());
    let mut parser = Parser::new(lexer);
    let program = parser.parse_program();

    assert_eq!(program.statements.len(), 3);

    let expected_identifiers = vec!["x", "y", "foobar"];
    for (index, expected_name) in expected_identifiers.iter().enumerate() {
        assert_let_statement(&program.statements[index], expected_name);
    }

    let errors = parser.errors();

    assert_eq!(errors.len(), 0);
}

#[test]
fn test_let_statement_errors() {
    // first line of input does not have an equal sign
    // last line does not have a semicolon
    let input = "
    let x 5 ;
    let y = 3";

    let lexer = Lexer::new(input.to_string());
    let mut parser = Parser::new(lexer);
    let _ = parser.parse_program();

    let errors = parser.errors();
    assert_eq!(errors.len(), 2);
}

fn assert_let_statement(statement: &Statement, expected_identifier: &str) {
    match statement {
        Statement::Let(identifier, _) => {
            assert_eq!(*identifier, expected_identifier.to_string());
        }
        _ => assert!(false),
    };
}

//________________________
// Returns
// ---------------------
#[test]
fn test_return_statement() {
    let input = "
    return 5;
    return 10;
    return 9999;
    ";

    let lexer = Lexer::new(input.to_string());
    let mut parser = Parser::new(lexer);
    let program = parser.parse_program();

    assert_eq!(program.statements.len(), 3);
    assert_eq!(parser.errors().len(), 0);

    for stmt in program.statements.iter() {
        match stmt {
            Statement::Return(_) => (),
            _ => assert!(false),
        };
    }
}

//________________________
// Expressions
// ---------------------

#[test]
fn test_identifier_statement() {
    let input = "foobar;";

    let lexer = Lexer::new(input.to_string());
    let mut parser = Parser::new(lexer);
    let program = parser.parse_program();

    assert_eq!(program.statements.len(), 1);
    assert_eq!(parser.errors().len(), 0);

    for stmt in program.statements.iter() {
        match stmt {
            Statement::Expression(expr) => match expr {
                Expression::Identifier(identifier) => {
                    assert_eq!(*identifier, "foobar".to_string());
                }
                _ => assert!(false),
            },
            _ => assert!(false),
        };
    }
}

#[test]
fn test_integer_literal_expression() {
    let input = "5;";

    let lexer = Lexer::new(input.to_string());
    let mut parser = Parser::new(lexer);
    let program = parser.parse_program();

    assert_eq!(program.statements.len(), 1);

    println!("{:?}", parser.errors());

    assert_eq!(parser.errors().len(), 0);

    for stmt in program.statements.iter() {
        match stmt {
            Statement::Expression(expr) => match expr {
                Expression::Integer(identifier) => {
                    assert_eq!(*identifier, 5);
                }
                _ => assert!(false),
            },
            _ => assert!(false),
        };
    }
}

#[test]
fn test_parsing_prefix_expressions() {
    // tests are tuples where
    // 0: the input, 1: the prefix operator, and 2: the token modified by the operator
    let tests = [("!5;", "!", int(5)), ("-15;", "-", int(15))];

    for test in tests.iter() {
        let lexer = Lexer::new(test.0.to_string());
        let mut parser = Parser::new(lexer);
        let program = parser.parse_program();
        assert_eq!(program.statements.len(), 1);
        println!("{:?}", parser.errors());

        assert_eq!(parser.errors().len(), 0);
        for stmt in program.statements.iter() {
            match stmt {
                Statement::Expression(expr) => match expr {
                    Expression::Prefix(operator, right) => {
                        assert_eq!(operator, test.1);
                        test_expected_expression(&right, &test.2)
                    }
                    _ => assert!(false),
                },
                _ => assert!(false),
            };
        }
    }
}

#[test]
fn test_parsing_infix_expressions() {
    // tests are tuples where
    // 0: the input, 1: the prefix operator, and 2: the token modified by the operator
    let tests = [
        (
            "5 + 5;",
            int(5),
            Token::new(TokenType::PLUS, "+".to_string()),
            int(5),
        ),
        (
            "5 - 5;",
            int(5),
            Token::new(TokenType::MINUS, "-".to_string()),
            int(5),
        ),
        (
            "5 * 5;",
            int(5),
            Token::new(TokenType::ASTERISK, "*".to_string()),
            int(5),
        ),
        (
            "5 / 5;",
            int(5),
            Token::new(TokenType::SLASH, "/".to_string()),
            int(5),
        ),
        (
            "5 > 5;",
            int(5),
            Token::new(TokenType::GT, ">".to_string()),
            int(5),
        ),
        (
            "5 < 5;",
            int(5),
            Token::new(TokenType::LT, "<".to_string()),
            int(5),
        ),
        (
            "5 == 5;",
            int(5),
            Token::new(TokenType::EQ, "==".to_string()),
            int(5),
        ),
        (
            "5 != 5;",
            int(5),
            Token::new(TokenType::NOTEQ, "!=".to_string()),
            int(5),
        ),
    ];

    for test in tests.iter() {
        let lexer = Lexer::new(test.0.to_string());
        let mut parser = Parser::new(lexer);
        let program = parser.parse_program();
        assert_eq!(program.statements.len(), 1);
        println!("{:?}", parser.errors());

        assert_eq!(parser.errors().len(), 0);
        for stmt in program.statements.iter() {
            match stmt {
                Statement::Expression(expr) => match expr {
                    Expression::Infix(operator, left, right) => {
                        assert!(operator == &test.2);
                        test_expected_expression(&left, &test.1);
                        test_expected_expression(&right, &test.3);
                    }
                    _ => assert!(false),
                },
                _ => assert!(false),
            };
        }
    }
}

#[test]
fn test_operator_precedence() {
    let input = vec![
        ("-a * b", "((-a) * b)"),
        ("!-a", "(!(-a))"),
        ("a + b + c", "((a + b) + c)"),
        ("a * b * c", "((a * b) * c)"),
        ("a * b / c", "((a * b) / c)"),
        ("a + b / c", "(a + (b / c))"),
        ("a + b * c + d / e - f", "(((a + (b * c)) + (d / e)) - f)"),
        ("3 + 4; -5 * 5", "(3 + 4)\n((-5) * 5)"),
        ("5 > 4 == 3 < 4", "((5 > 4) == (3 < 4))"),
        ("5 < 4 != 3 > 4", "((5 < 4) != (3 > 4))"),
        (
            "3 + 4 * 5 == 3 * 1 + 4 * 5",
            "((3 + (4 * 5)) == ((3 * 1) + (4 * 5)))",
        ),
        (
            "3 + 4 * 5 == 3 * 1 + 4 * 5",
            "((3 + (4 * 5)) == ((3 * 1) + (4 * 5)))",
        ),
    ];

    for test in input.iter() {
        let lexer = Lexer::new(test.0.to_string());
        let mut parser = Parser::new(lexer);

        let program = parser.parse_program();

        assert_eq!(parser.errors().len(), 0);

        println!("{:?}", program.to_string());

        assert_eq!(program.to_string(), test.1);
    }
}

//------------------------
// Helper Functions
//------------------------
/// test_expected_expression is a helper function that takes a
/// given expression and compares it to an expected expression
fn test_expected_expression(expr: &Expression, expected: &Expression) {
    match expr {
        Expression::Identifier(_) => match expected {
            Expression::Identifier(ident) => test_identifier(expr, &ident),
            _ => assert!(false),
        },
        Expression::Integer(_) => match expected {
            Expression::Integer(i) => test_integer_literal(expr, *i),
            _ => assert!(false),
        },
        Expression::Bool(val) => match expected {
            Expression::Bool(val2) => assert_eq!(val, val2),
            _ => assert!(false),
        },
        _ => assert!(false),
    }
}

/// bool is a helper function that creates a Monkey Boolean Expression
/// with a given bool
fn bool(value: bool) -> Expression {
    Expression::Bool(value)
}

/// int is a helper function that creates a Monkey Integer Expression
/// with a given i32
fn int(value: i32) -> Expression {
    Expression::Integer(value)
}

/// test_integer_literal is a helper that takes an Integer expression
/// and asserts that it is equal to a given i32
fn test_integer_literal(expr: &Expression, expected: i32) {
    match expr {
        Expression::Integer(i) => assert_eq!(*i, expected),
        _ => assert!(false),
    }
}

/// test_identified is a helper function that takes an Identifier expression
/// and asserts that its literal value matches the expected String
fn test_identifier(expr: &Expression, expected: &String) {
    match expr {
        Expression::Identifier(ident) => assert_eq!(ident, expected),
        _ => assert!(false),
    }
}
