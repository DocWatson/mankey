use std::fmt;

#[derive(Debug, PartialEq, PartialOrd)]
pub enum Precedence {
    LOWEST,
    EQUALS,      // == or !=
    LESSGREATER, // >= or <=
    SUM,         // + or -
    PRODUCT,     // * or /
    PREFIX,      // +x or -x
    CALL,        // function(y)
}

#[derive(Debug, PartialEq, Clone)]
pub struct Token {
    pub token_type: TokenType,
    pub literal: String,
}

impl Token {
    pub fn new(token_type: TokenType, literal: String) -> Token {
        Token {
            token_type: token_type,
            literal: literal,
        }
    }
    pub fn get_precendence(&self) -> Precedence {
        match self.token_type {
            TokenType::EQ => Precedence::EQUALS,
            TokenType::NOTEQ => Precedence::EQUALS,
            TokenType::LT => Precedence::LESSGREATER,
            TokenType::GT => Precedence::LESSGREATER,
            TokenType::LTE => Precedence::LESSGREATER,
            TokenType::GTE => Precedence::LESSGREATER,
            TokenType::PLUS => Precedence::SUM,
            TokenType::MINUS => Precedence::SUM,
            TokenType::ASTERISK => Precedence::PRODUCT,
            TokenType::SLASH => Precedence::PRODUCT,
            _ => Precedence::LOWEST,
        }
    }
}

pub fn lookup_identifier(word: &str) -> TokenType {
    match word {
        "fn" => TokenType::FUNCTION,
        "let" => TokenType::LET,
        "type" => TokenType::TYPE,
        "impl" => TokenType::IMPL,
        "if" => TokenType::IF,
        "else" => TokenType::ELSE,
        "elseif" => TokenType::ELSEIF,
        "return" => TokenType::RETURN,
        "true" => TokenType::TRUE,
        "false" => TokenType::FALSE,
        _ => TokenType::IDENT,
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum TokenType {
    ILLEGAL,
    EOF,

    // Identifiers
    IDENT, // Add, foobar, x, y, etc
    INT,   // Integers such as 1, 33 or 89
    FLOAT, // Floats such as 1.5, 10.2
    STRING,

    // Operators
    ASSIGN,   // '='
    PLUS,     // '+'
    MINUS,    // '-'
    BANG,     // '!'
    ASTERISK, // '*'
    SLASH,    // '/'
    MODULO,   // '%'
    QUESTION, // '?'

    LT,  // '<'
    LTE, // <=
    GT,  // '>'
    GTE, // >=

    EQ,    // '=='
    NOTEQ, // '!='

    // Delimiters
    COMMA,     // ','
    SEMICOLON, // ';'
    COLON,     // ':'
    ROCKET,    // =>

    LPAREN,   // '('
    RPAREN,   // ')'
    LBRACE,   // '{'
    RBRACE,   // '}'
    LBRACKET, // '['
    RBRACKET, // ']'

    // General Keywords
    FUNCTION, // Function declaration
    LET,      // Variable declaration
    RETURN,   // Return declaration
    TYPE,     // Type declaration
    IMPL,     // Implementation declaration

    // Conditional Statement Keywords
    IF,     // If statement
    ELSE,   // Else statement
    ELSEIF, // Elseif statement
    MATCH,  // Match statement

    // Boolean Keywords
    TRUE,  // true boolean
    FALSE, // false boolean
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), ::std::fmt::Error> {
        match self.token_type {
            TokenType::ILLEGAL => write!(f, "None"),
            _ => write!(f, "{}", self.literal),
        }
    }
}
